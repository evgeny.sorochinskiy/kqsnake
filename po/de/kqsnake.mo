��    +      t  ;   �      �     �     �     �     �             '   1  $   Y     ~  7   �     �      �     �     �  "     I   '     q     �     �     �     �     �     �               (  
   F     Q     f     z     �     �     �     �     �  O   �     6     E     N     V     _     o  m  ~     �     �      	  "   $	     G	     \	  1   e	  1   �	     �	  0   �	     
  .   
  
   ?
  
   J
  2   U
  D   �
     �
     �
     �
            "   8     [     w     }  &   �     �     �     �      �          =     R     i     �  t   �     
          (     /     C     X                                        &              )                  (       '          %                *          !   
                  "                       $      +         #          	             &Game Already Running Another Game Of Snake under KDE Application Already Running Back And Fore Colors Background Color Change Snake Movement Speed dynamically Clear the field and begin a new game Colors Control the snake and collect as many apples as you can Created by:  EMAIL OF TRANSLATORSYour emails Foreground Color General Launch %1 -h or %1 --help for help Launch this application without any parameters to see its main functional Length and moves Main Toolbar NAME OF TRANSLATORSYour names New Game Number of moves between apples Number of moves between maluses Open settings dialog Original author Show maluses Simple Snake Game realization Snake Game Snake Movement Speed Snake length to win Sorry, you lost Start or stop current game Start/Stop Game Unknown option %1 Usage %1 [options] Use maluses Use right and left arrow keys or A/D keys or mouse buttons to control the snake Valid options: You lost You win You win! display version show this help Project-Id-Version: kqsnake
Report-Msgid-Bugs-To: https://bugs.kde.org
PO-Revision-Date: 2023-11-21 14:36+0300
Last-Translator: Eugene Sorochinskiy <webmaster@darkguard.net>
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 &Spiel Schon gestartet Das andere Schlangespiel mit KDE Die Anwendung  ist schon gestartet Vor- und Rückfarben Vorfarbe Die Schlangegeschwindigkeit verändern beim Spiel Das Feld freiräumen und ein neues Spiel anfangen Farben Die Schlange kontrolieren und die Äpfel sammeln Erstellt von:  webmaster@darkguard.net, manager@darkguard.net Rückfarbe Allgemeine Starten %1 -h oder %1 --help, um Hilfe zu erhalten Starten die Anwendung  ohne Argumente, um die Hauptfunktion zu sehen Langheit und Bewegungen Hauptsymbolleiste Eugen Soroshchinskii Neues Spiel Bewegunganzahl zwischen Äpfeln Bewegunganzahl zwischen Schlechten Besetzungefenster aufmachen Autor Schlechte anzeigen Die einfache Schlangespiel realization Schlangespiel Schlangelaufgeschwindigkeit Schlangelangheit zum Gewinn Tut man leid, haben Sie verloren Start/Stop  das aktuelle Spiel Start/Stop das Spiel Unbekannte Argument %1 Benutzen: %1 [Argumente] Schlechte benutzen Benutzen Sie Links-und-Rechtspfeiltasten oder A/D-Tasten oder Links-und-Rechtsmausklicke, um die Schlange zu steuern Gültige Argumenten: Verloren Gewinn Haben Sie gewinnen! die Version anzeigen diese Hilfe anzeigen 