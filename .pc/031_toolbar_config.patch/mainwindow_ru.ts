<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="configdialog.ui" line="14"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_configdialog.h" line="90"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_configdialog.h" line="90"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="22"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_configdialog.h" line="91"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_configdialog.h" line="91"/>
        <source>Snake Length To Win</source>
        <translation>Длина для выигрыша</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="36"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_configdialog.h" line="92"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_configdialog.h" line="92"/>
        <source>Moves Between Apples</source>
        <translation>Ходов перед появлением яблока</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="138"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="138"/>
        <source>Snake Game</source>
        <translation>Игра &quot;Змейка&quot;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="156"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="156"/>
        <source>Game</source>
        <translation>Игра</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="157"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="157"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="113"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="139"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="139"/>
        <source>New Game</source>
        <translation>Новая игра</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="116"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="141"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="141"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="121"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="143"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="143"/>
        <source>About Qt</source>
        <translation>О программе Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="126"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="144"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="144"/>
        <source>About</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="135"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="145"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="145"/>
        <source>Start/Stop Game</source>
        <translation>Старт/Стоп</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="147"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="147"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="147"/>
        <location filename="mainwindow.ui" line="150"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="149"/>
        <location filename="cmake-build-debug/kqsnake_autogen/include/ui_mainwindow.h" line="151"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="149"/>
        <location filename="cmake-build-release/kqsnake_autogen/include/ui_mainwindow.h" line="151"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="29"/>
        <source>Snake Movement Speed</source>
        <oldsource>Generation Change Speed</oldsource>
        <translation>Скорость движения</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="36"/>
        <source>Use right and left arrow keys or A/D keys or mouse buttons to control the snake</source>
        <translation>Управляйте змейкой при помощи клавиш &quot;влево&quot; и &quot;вправо&quot;, клавиш A/D или левой и правой кнопок мыши</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="54"/>
        <source>A simple Game Of Snake</source>
        <translation>Простая игра &quot;Змейка&quot;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="snakegame.cpp" line="200"/>
        <source>You lost</source>
        <translatorcomment>Вы проиграли</translatorcomment>
        <translation>Поражение</translation>
    </message>
    <message>
        <location filename="snakegame.cpp" line="200"/>
        <source>Sorry, you lost</source>
        <translation>Вы проиграли</translation>
    </message>
    <message>
        <location filename="snakegame.cpp" line="211"/>
        <source>You win</source>
        <translation>Выигрыш</translation>
    </message>
    <message>
        <location filename="snakegame.cpp" line="211"/>
        <source>You win!</source>
        <translation>Вы выиграли!</translation>
    </message>
    <message>
        <location filename="main.cpp" line="29"/>
        <source>Simple Snake Game realization</source>
        <oldsource>Simple Game Of Life realization</oldsource>
        <translation>Еще одна реализация игры &quot;Змейка&quot;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="30"/>
        <source>Created by: </source>
        <translation>Автор: </translation>
    </message>
    <message>
        <location filename="main.cpp" line="34"/>
        <source>Usage %1 [options]</source>
        <translation>Использование %1 [параметры]</translation>
    </message>
    <message>
        <location filename="main.cpp" line="36"/>
        <source>Valid options:</source>
        <translation>Параметры:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="37"/>
        <source>show this help</source>
        <translation>показать инструкцию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="38"/>
        <source>display version</source>
        <translation>показать версию</translation>
    </message>
    <message>
        <location filename="main.cpp" line="43"/>
        <source>Unknown option %1</source>
        <translation>Неизвестный параметр %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="44"/>
        <source>Launch %1 -h or %1 --help for help</source>
        <translation>Запустите %1 -h или %1 --help для получения инструкции</translation>
    </message>
    <message>
        <location filename="main.cpp" line="48"/>
        <source>Launch this application without any parameters to see its main functional</source>
        <translation>Запустите без параметров для использования по прямому назначению</translation>
    </message>
</context>
</TS>
